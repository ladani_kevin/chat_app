import firebase from 'firebase'
import firestore from 'firebase/firestore'

  // Initialize Firebase
  const firebaseConfig = {
    apiKey: "AIzaSyAt_ORBXguBEtWtjGEtLnPy0sDDdFkvoRw",
    authDomain: "chat-app-92271.firebaseapp.com",
    databaseURL: "https://chat-app-92271.firebaseio.com",
    projectId: "chat-app-92271",
    storageBucket: "chat-app-92271.appspot.com",
    messagingSenderId: "399662075886",
    appId: "1:399662075886:web:446c134c5dbe92812508ff",
    measurementId: "G-2Z62V72D27"
  };
  
  const firebaseApp = firebase.initializeApp(firebaseConfig);
  firebaseApp.firestore().settings({ timestampsInSnapshots: true })

  export default firebaseApp.firestore()
